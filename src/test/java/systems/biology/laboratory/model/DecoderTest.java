package systems.biology.laboratory.model;

import org.junit.Test;
import systems.biology.laboratory.utils.APUChunkDecoder;

import static junit.framework.TestCase.assertEquals;

public class DecoderTest {

    @Test
    public void basicDecodingTest(){

        int[] values = new int[]{0,2,0,0,0,0,0,0,0,5,10,2,23,0,0,0,0,0,1,1,1,1,1,0,0,20345,67605,0,0,3000,3000,3000,400,401,402,403,404,405,406,407,70000,605,606,909,400,300,280,290,350};

        //byte encoded rep of the values above
        byte[] arr = new byte[]{84,0,0,0,1,0,0,0,1,86,109,109,48,49,0,88,101,8,112,117,50,46,76,69,46,49,1,0,0,0,87,0,0,0,0,0,0,0,0,7,-48,89,0,0,8,1,65,73,2,66,7,82,4,5,10,2,23,66,5,70,5,66,2,77,121,79,90,0,1,8,21,66,2,78,3,-72,11,77,-112,1,77,-111,1,77,-110,1,77,-109,1,77,-108,1,77,-107,1,77,-106,1,77,-105,1,90,0,1,17,112,77,93,2,77,94,2,77,-115,3,77,-112,1,77,44,1,77,24,1,77,34,1,77,94,1,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48};

        DecodingResult result = APUChunkDecoder.decodeBinarySPECtoRAW(arr);

        SpecChunk specChunk = APUChunkDecoder.createACH(result);

        for(int i = 0; i < values.length; i++){
            assertEquals(values[i], specChunk.getHistogram()[i]);
        }


    }

}

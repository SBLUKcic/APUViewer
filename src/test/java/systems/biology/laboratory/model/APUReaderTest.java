package systems.biology.laboratory.model;

import org.junit.Test;
import systems.biology.laboratory.utils.APUReader;

import java.io.File;
import java.util.Map;

import static org.junit.Assert.*;

public class APUReaderTest {

    private File apu = new File("/home/pear/IdeaProjects/APUViewer/src/test/resources/xsbf161.r1r2.43bp.breakseq.excl.pu1.LE.1.apu");

    @Test
    public void testAPUPointerReader() throws Exception{
        APUReader apuReader = new APUReader();
        Map<String, PointerChunk> chunks = apuReader.extractPointers(apu);
        assertEquals(78, chunks.size());
    }

}
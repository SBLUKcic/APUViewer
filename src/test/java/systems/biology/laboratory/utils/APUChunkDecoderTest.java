package systems.biology.laboratory.utils;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.stream.IntStream;

public class APUChunkDecoderTest {

    @Test
    public void intToCharArrayTESt(){
        char[] chars = new char[3];
        addChars(chars);

        System.out.println(String.valueOf(chars));
    }

    private void addChars(char[] chars) {
        for(int i = 0; i < chars.length; i++){
            chars[i] = 'B';
        }
    }

    @Test
    public void byteArrayToCharArrayTest(){
        byte[] bytes = new byte[]{125, 10};
        int val = 2685;

        long s;
        long e;

        s = System.nanoTime();
        int d = convertFromByteArray2(bytes[0], bytes[1]);
        char[] c = String.valueOf(d).toCharArray();
        e = System.nanoTime();
        System.out.println("1: " + String.valueOf(e - s));

        s = System.nanoTime();
        char[] f = bytestoCharArr(bytes[0], bytes[1]);
        e = System.nanoTime();
        System.out.println("2: " + String.valueOf(e - s));

        System.out.println(String.valueOf(c));
        System.out.println(String.valueOf(f));

    }

    @Test
    public void StringUtilTest(){

        long s;
        long e;

        s = System.nanoTime();
        String str = String.format("%05d", 256);
        e = System.nanoTime();

        System.out.println("1: " + String.valueOf(e - s));
        System.out.println(str);

        s = System.nanoTime();
        String str2 = StringUtils.leftPad(String.valueOf(256), 5, '0');
        e = System.nanoTime();

        System.out.println("2: " + String.valueOf(e - s ));
        System.out.println(str2);

        s = System.nanoTime();
        String str3 = getPaddedString(String.valueOf(2), 3);
        e = System.nanoTime();

        System.out.println("3: " + String.valueOf(e - s ));
        System.out.println(str3);

    }

    public static String getPaddedString(String s, int max){
        StringBuilder b = new StringBuilder(max);
        for(int i = 0; i < max - s.length(); i++){
            b.append('0');
        }
        b.append(s);
        return b.toString();
    }

    static int convertFromByteArray2(byte byte1, byte byte2){
        return ((byte2 & 0xFF) << 8 | (byte1 & 0xFF));
    }

    static char[] bytestoCharArr(byte byte1, byte byte2){
        return String.valueOf(((byte2 & 0xFF) << 8 | (byte1 & 0xFF))).toCharArray();
    }






}
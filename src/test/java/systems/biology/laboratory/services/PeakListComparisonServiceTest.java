package systems.biology.laboratory.services;


import javafx.application.Application;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import systems.biology.laboratory.model.Peak;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;


public class PeakListComparisonServiceTest extends Application {

    private CountDownLatch lock = new CountDownLatch(1);
    private List<Peak> peaks;

    @BeforeClass
    public static void setUpClass() throws InterruptedException{
        Thread t = new Thread("JavaFX init thread"){
            public void run(){
                Application.launch(PeakListComparisonServiceTest.class, new String[0]);
            }
        };
        t.setDaemon(true);
        t.start();

    }

    @Test
    public void peakTest() throws InterruptedException{

        List<Peak> p1 = new ArrayList<>();
        p1.add(new Peak(120, 170, 220, 100, "mm01"));
        p1.add(new Peak(100, 200, 150, 100, "mm01"));


        List<Peak> p2 = new ArrayList<>();
        p2.add(new Peak(100, 200, 150, 5, "mm01"));
        p2.add(new Peak(120, 170, 220, 9, "mm01"));

        PeakListComparisonService service = new PeakListComparisonService(p1, p2);

        service.setOnSucceeded(e -> {
            System.out.println("done");
            peaks = service.getValue();
            lock.countDown();
        });

        service.start();


        lock.await();

        assertEquals(2, peaks.size());

        //fail("something");



        System.out.println("End of test");

    }

    @After
    public void clearLock(){
        lock.countDown();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}

package systems.biology.laboratory.services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import systems.biology.laboratory.components.APUFileDetailsModal;
import systems.biology.laboratory.utils.APUReader;
import systems.biology.laboratory.model.Peak;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.model.SpecChunk;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class PeakFinderService extends Service<List<Peak>> {

    private Map<String, PointerChunk> pointerChunks;
    private File APUFile;

    public PeakFinderService(Map<String, PointerChunk> pointerChunks, File APUFile) {
        this.pointerChunks = pointerChunks;
        this.APUFile = APUFile;
    }

    @Override
    protected Task<List<Peak>> createTask() {

        return new Task<List<Peak>>(){
            protected List<Peak> call() throws Exception {

                List<Peak> HighPeaks = new ArrayList<>();

                int cores = 10;
                ThreadPoolExecutor executor = new ThreadPoolExecutor(cores, cores, Integer.MAX_VALUE, TimeUnit.SECONDS, new LinkedBlockingQueue<>(Integer.MAX_VALUE));
                CompletionService<List<Peak>> HighestResPeaksCompletionService = new ExecutorCompletionService<>(executor);

                Future<List<Peak>> peakFuture;

                int threadCount = 0;
                int threadsCompleted = 0;

                int max = pointerChunks.size() / 6;

                updateMessage("Decoding chunks..");

                for (Map.Entry<String, PointerChunk> chunk : pointerChunks.entrySet()) {
                    if (chunk.getValue().getBinSize() == 1 && chunk.getValue().getStepSize() == 1) {
                        //highest res.
                        byte[] bytes = APUReader.readAPUChunk(APUFile, chunk.getValue().getBytePosition());
                        PeakFinderCallable peakFinderCallable = new PeakFinderCallable(chunk.getValue(), bytes);
                        HighestResPeaksCompletionService.submit(peakFinderCallable);
                        threadCount++;

                        updateProgress(threadCount, max);
                    }

                }

                updateMessage("Loading peaks..");
                updateProgress(0, max);

                while (threadsCompleted < threadCount) {
                    try {
                        peakFuture = HighestResPeaksCompletionService.take();
                        try {
                            if (peakFuture.get() != null) {
                                HighPeaks.addAll(peakFuture.get());
                            }
                            threadsCompleted++;
                            //update the caller
                            updateProgress(threadsCompleted, threadCount);
                        } catch (Exception e) {
                            System.out.println("completion error: " + e.getMessage());
                        }
                    } catch (InterruptedException ie) {
                        System.out.println(ie.getMessage());
                    }
                }

                return HighPeaks;
            }
        };

    }
}

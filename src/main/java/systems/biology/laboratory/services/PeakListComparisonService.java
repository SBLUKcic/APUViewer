package systems.biology.laboratory.services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import systems.biology.laboratory.model.Peak;

import java.util.ArrayList;
import java.util.List;

public class PeakListComparisonService extends Service<List<Peak>> {

    private List<Peak> p1;
    private List<Peak> p2;
    private int threshold;

    public PeakListComparisonService(List<Peak> peakListOne, List<Peak> peakListTwo, int threshold) {
        this.p1 = peakListOne;
        this.p2 = peakListTwo;
        this.threshold = threshold;
    }

    @Override
    protected Task<List<Peak>> createTask() {

        return new Task<List<Peak>>() {
            @Override
            protected List<Peak> call() throws Exception {

                System.out.println("Finding peaks");


                p1.sort((a, b) -> {
                    int comp = Integer.compare(a.getPeakMaxima(), b.getPeakMaxima());
                    return comp != 0 ? comp : a.getRname().compareTo(b.getRname());
                });

                p2.sort((a, b) -> {
                    int comp = Integer.compare(a.getPeakMaxima(), b.getPeakMaxima());
                    return comp != 0 ? comp : a.getRname().compareTo(b.getRname());
                });

                List<Peak> interestingPeaks = new ArrayList<>();

                int search = p2.size() < p1.size() ? p2.size() : p1.size();
                for (int i=0; i < search; i++) {
                    for(int j=i; j < search; j++){
                        if(p1.get(i).equals(p2.get(j))){
                            if((p1.get(i).getPeakHeight() / p2.get(j).getPeakHeight() >= threshold) || (p2.get(j).getPeakHeight() / p1.get(i).getPeakHeight() >= threshold)){
                                interestingPeaks.add(p1.get(i));
                                break;
                            }
                        }else if(p2.get(j).getPeakMaxima() > p1.get(i).getPeakMaxima()) break;
                    }
                }

                System.out.println("Found peaks: " + interestingPeaks.size());

                return interestingPeaks;
            }
        };
    }
}

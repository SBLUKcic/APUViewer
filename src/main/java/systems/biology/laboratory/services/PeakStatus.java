package systems.biology.laboratory.services;

public enum PeakStatus {
    Climbing, Descending
}

package systems.biology.laboratory.services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import systems.biology.laboratory.model.DecodingResult;
import systems.biology.laboratory.utils.ACHBuilder;
import systems.biology.laboratory.utils.APUReader;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.model.SpecChunk;
import systems.biology.laboratory.utils.Decoder;


import java.io.File;


public class APUChunkReaderService extends Service<SpecChunk> {

    private PointerChunk pc;
    private File APUFile;

    public APUChunkReaderService(PointerChunk pc, File APUFile) {
        this.pc = pc;
        this.APUFile = APUFile;
    }

    @Override
    protected Task<SpecChunk> createTask() {
        return new Task<SpecChunk>(){

            @Override
            protected SpecChunk call() throws Exception {
                int bytePosition = pc.getBytePosition();
                DecodingResult result = Decoder.decodeBinarySPECtoRAW(APUReader.readAPUChunk(APUFile, bytePosition));
                return ACHBuilder.createACH(result);
            }
        };
    }
}

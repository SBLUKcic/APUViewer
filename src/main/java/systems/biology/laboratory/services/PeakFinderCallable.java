package systems.biology.laboratory.services;

import systems.biology.laboratory.model.DecodingResult;
import systems.biology.laboratory.model.Peak;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.model.SpecChunk;
import systems.biology.laboratory.utils.ACHBuilder;
import systems.biology.laboratory.utils.Decoder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class PeakFinderCallable implements Callable<List<Peak>> {


    private PointerChunk chunk;
    private byte[] bytes;

    PeakFinderCallable(PointerChunk chunk, byte[] bytes){
        this.chunk = chunk;
        this.bytes = bytes;
    }

    @Override
    public List<Peak> call() throws Exception {
        try {
            DecodingResult result = Decoder.decodeBinarySPECtoRAW(bytes);
            SpecChunk highRes = ACHBuilder.createACH(result);
            return findPeaks(highRes.getHistogram(), highRes.getStartPos(), chunk.getRname());
        }catch (Exception e){
            System.out.println("Peaking finding call error: " + e.getMessage());
        }
        return new ArrayList<>();
    }

     List<Peak> findPeaks(int[] map, int startPos, String rname){
        List<Peak> peaks = new ArrayList<>();

        int threshold = 30;

        PeakStatus current_state = PeakStatus.Climbing;
        PeakStatus next_input;


        for(int i = 0; i < map.length-1; i++){
            //determine next input
            if(i == map.length - 1){
                next_input = PeakStatus.Descending;
            }else{
                if(map[i] < map[i+1] && map[i+1] > map[i]/2){
                    next_input = PeakStatus.Climbing;
                }else{
                    next_input = PeakStatus.Descending;
                }
            }

            if(current_state == PeakStatus.Climbing && next_input == PeakStatus.Descending){
                if(map[i] > threshold) {

                    int maxima = findLocalMaxima(i, map);
                    int secondThresh = map[maxima]/3;
                    int right = findRightPeakEdge(maxima, map, secondThresh);
                    peaks.add(new Peak(findLeftPeakEdge(maxima, map, secondThresh) + startPos, right + startPos, maxima + startPos, map[i], rname));
                    i = right;
                }
            }

            current_state = next_input;
        }

        return peaks;
    }

    private int findLocalMaxima(int pos, int[] map) {

        int max = map[pos];
        int maxPos = pos;
        for(int i = pos+1; i < map.length-1; i++ ){

            if(map[i] > max){
                max = map[i];
                maxPos = i;
            }

            if(map[i] < 10  || map[i] < max/3){
                return maxPos;
            }
        }

        return map.length-1;
    }

    private int findRightPeakEdge(int pos, int[] map, int threshold) {

        for(int i = pos; i < map.length-1; i++ ){

            if(map[i+1] < 10 || map[i+1] < threshold){
                return i + 1 < map.length -1 ? i +1 : map.length-1;
            }
        }

        return map.length-1;
    }

    private int findLeftPeakEdge(int pos, int[] map, int threshold) {

        for(int i = pos; i > 0; i-- ){

            if(map[i-1] < 10  || map[i-1] < threshold){
               return i - 1 > 0 ? i - 1 : 0;
            }
        }

        return 0;
    }
}

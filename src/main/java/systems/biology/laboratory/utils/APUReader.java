package systems.biology.laboratory.utils;

import systems.biology.laboratory.model.DecodingResult;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.model.SpecChunk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.*;

/** Contains methods to handle and extract data from valid APU files.
 *
 */
public class APUReader {


    /** Extracts the pointer section from valid APUfile. Stores each section details in a PointerChunk object.
     * @param file APU file to read
     * @return Map of string to pointchunk, key is id, ie bin_size_rname, 10_10_mm01, value is the pointer chunk
     */
    public static Map<String, PointerChunk> extractPointers(File file) {

        try {

            ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 200, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
            CompletionService<Map<String, PointerChunk>> cs = new ExecutorCompletionService<>(executor);

            Future<Map<String, PointerChunk>> mapFuture = new CompletableFuture<>();

            cs.submit(new LoadPointersTask(file));

            int count = 0;

            while(count < 1){
                try{
                    mapFuture = cs.take();
                    count++;
                }catch (InterruptedException e){
                    System.out.println(e.getMessage());
                }
            }

            return mapFuture.get();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return null;
    }

    /** Loading task extracted into a callable, to run multithread.
     *
     */
    static class LoadPointersTask implements Callable<Map<String, PointerChunk>>{

        private File file;

        public LoadPointersTask(File file){
            this.file = file;
        }

        @Override
        public Map<String, PointerChunk> call() throws Exception {
            Map<String, PointerChunk> chunks = new HashMap<>();
            //TODO: add more chunks, search for _1, _2, _3 and _4. not just the rname. This will ignore reads
            FileInputStream fis = new FileInputStream(file);
            //skip past the first 8 bytes
            //TODO: maybe implement some header checker - to read in info from that - check valid
            byte[] header = new byte[8];
            fis.read(header);

            byte[] TYPE = new byte[4];
            fis.read(TYPE);
            if (!checkPointerTypeIsValid(TYPE)) throw new IOException("Pointer chunk invalid");

            byte[] SIZE = new byte[4];
            fis.read(SIZE);
            int noBytes = convertFromByteArray4(SIZE);

            byte[] allChunks = new byte[noBytes];
            fis.read(allChunks);

            for (int i = 0; i < allChunks.length; i++) {
                i += 3; //skip the res
                //parse the step size;
                StringBuilder stepSizeBuilder = new StringBuilder();
                for (int j = 0; j < 10; j++) {
                    stepSizeBuilder.append((char) allChunks[i + j]);
                }
                i += 10;

                int stepSize = Integer.parseInt(stepSizeBuilder.toString().replaceFirst("^0+(?!$)", ""));


                //parse the bin size

                StringBuilder binSizeBuilder = new StringBuilder();
                for (int j = 0; j < 10; j++) {
                    binSizeBuilder.append((char) allChunks[i + j]);
                }
                i += 10;

                int binSize = Integer.parseInt(binSizeBuilder.toString());

                //skip the Z
                i += 1;

                //get the rname length
                StringBuilder rnameLengthBuilder = new StringBuilder();
                for (int j = 0; j < 3; j++) {
                    rnameLengthBuilder.append((char) allChunks[i + j]);
                }
                i += 3;

                int len = Integer.parseInt(rnameLengthBuilder.toString());

                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < len; j++) {
                    sb.append((char) allChunks[i + j]);
                }
                i += len;

                //parse the byte position

                StringBuilder byteSizeBuilder = new StringBuilder();
                for (int j = 0; j < 10; j++) {
                    byteSizeBuilder.append((char) allChunks[i + j]);
                }
                i += 9;

                int bytePosition = Integer.parseInt(byteSizeBuilder.toString().replaceFirst("^0+(?!$)", ""));

                PointerChunk chunk = new PointerChunk(stepSize, binSize, sb.toString(), bytePosition);
                chunks.put(PointerChunk.createId(stepSize, binSize, chunk.getRname()), chunk);
            }

            return chunks;
        }
    }


    /** Checks the Pointers TYPE is valid. checks against ASCII characters 'P','N','T','R'.
     * @param TYPE bytes to check
     * @return true or false if match
     */
    private static boolean checkPointerTypeIsValid(byte[] TYPE) {
        return TYPE[0] == 80 && TYPE[1] == 78 && TYPE[2] == 84 && TYPE[3] == 82;
    }

    /** Converts from 4 bytes into a 32 bit unsigned integer
     * @param bytes byte array to convert
     * @return integer representation of bytes
     */
    public static int convertFromByteArray4(byte[] bytes){
        return ByteBuffer.wrap(bytes).getInt();
    }

    /** Takes in a byte position and an APU file and returns the APU chunk as a Map
     * @param apuFile file to read
     * @param bytePosition starting position of the apu chunk
     * @return Map of Integer - Integer. Key is the bin position, value is the bin frequency
     */
    public static byte[] readAPUChunk(File apuFile, int bytePosition) {

        //read past header and pointer
        byte[] header = new byte[8];
        byte[] pntrtype = new byte[4];
        byte[] pntrsize = new byte[4];

        try{
            FileInputStream fis = new FileInputStream(apuFile);

            fis.read(header);
            fis.read(pntrtype);
            fis.read(pntrsize);
            int pntrBytes = APUReader.convertFromByteArray4(pntrsize);

            byte[] skip = new byte[pntrBytes];
            fis.read(skip);

            byte[] skip2 = new byte[bytePosition];
            fis.read(skip2);

            //now at the chunk
            byte[] chunkTYPE = new byte[4];
            fis.read(chunkTYPE);
            byte[] chunkSIZE = new byte[4];
            fis.read(chunkSIZE);

            int chunksize = APUReader.convertFromByteArray4(chunkSIZE);

            byte[] chunk = new byte[chunksize];
            fis.read(chunk);

            fis.close();
            return chunk;

        }catch (IOException ioe){
            System.out.println(ioe.getMessage());
        }

        return new byte[]{0};
    }

}

package systems.biology.laboratory.utils;

import systems.biology.laboratory.model.DecodingResult;
import systems.biology.laboratory.model.Footprintcode;
import systems.biology.laboratory.model.SpecChunk;
import systems.biology.laboratory.model.Token;

public class ACHBuilder {

    /** Takes in a histogram in string format and creates a SpecChunk
     *  Specchunk contains and int[] of values, and a start position.
     * @param decodingResult result from the decoding
     * @return Object containing body and startposition
     */
    public static SpecChunk createACH(DecodingResult decodingResult) {

        char[] values = decodingResult.getCode();
        int len = decodingResult.getHistogramLength();
        //Map<Integer, Integer> ACH = new TreeMap<>();
        int[] ach = new int[len];
        String footprintDesc = "";
        //get the first position on the rname.
        boolean doesItHit = false;

        int startPos = 0;
        int currentPos = 0;
        boolean startFound = false;
        int endPos;
        String footprintCode = "";
        int i;
        try {

            for (i = 0; i < values.length; i++) {

                //loop through until W -> get the position
                if (values[i] == 'W' && !startFound) {

                    //skip the 18 0's
                    startPos = charArrayToInt(values, i+18, i+29); //watch for the end val - might need to be 28 or 30
                    // currentPos = startPos;
                    i += 28;
                    startFound = true;
                }

                if(values[i] == 'Y' && startFound){
                    endPos = charArrayToInt(values, i+1, i+11);
                    i+=11;
                }

                if(values[i] == 'X'){
                    int code = charArrayToInt(values, i+1, i+3);
                    for(Footprintcode fCode : Footprintcode.values()){
                        if(fCode.getCode() == code){
                            footprintCode = fCode.getTranslation();
                        }
                    }
                    //do something with the code??
                    int num = charArrayToInt(values, i+4, i+7);
                    i += 7;
                    footprintDesc = new String(values).substring(i, i + num);
                    i += num - 1;
                    doesItHit = true;
                }

                if(values[i] == 'V'){

                    //skip past the RNAME, do with it what you will
                    while(values[i+1] != '0' && values[i+2] != '0' && values[i+3] != '0'){
                        i++;
                    }

                    i+=2;
                }

                //loop past Y
                if (startFound && ((values[i] >= 'A' && values[i] <= 'S') || values[i] == 'Z')) {
                    char character = values[i];
                    if (character != 'S' && character != 'R') {

                        int length = 1;
                        int frequency = 0;
                        int lengthBits = 0;
                        int heightBits = 0;
                        for (Token token : Token.values()) {
                            if (token.name().equals(String.valueOf(character))) {
                                lengthBits = token.getLength();
                                heightBits = token.getHeight();
                            }
                        }

                        switch (lengthBits) {
                            case 1:
                                length = 1;
                                break;
                            case 8:
                                length = charArrayToInt(values, i+1, i+4);
                                i += 3;
                                break;
                            case 16:
                                length = charArrayToInt(values, i+1, i+6);
                                i += 5;
                                break;
                            case 32:
                                length = charArrayToInt(values, i+1, i+11);
                                i += 10;
                        }

                        switch (heightBits) {
                            case 1:
                                frequency = 1;
                                break;
                            case 8:
                                frequency = charArrayToInt(values, i+1, i+4);
                                i += 3;
                                break;
                            case 16:
                                frequency = charArrayToInt(values, i+1, i+6);
                                i += 5;
                                break;
                            case 32:
                                frequency = charArrayToInt(values, i+1, i+11);
                                i += 10;
                        }

                        for (int j = 0; j < length; j++) {
                            ach[currentPos] = frequency;
                            currentPos++;
                        }

                    } else {
                        if (character == 'R') {
                            int numBins = charArrayToInt(values, i+1, i+4);
                            i += 3;
                            for (int j = 0; j < numBins; j++) {
                                ach[currentPos] = charArrayToInt(values, i+1, i+4);
                                currentPos++;
                                i += 3;
                            }

                        } else {
                            int numBins = charArrayToInt(values, i+1, i+4);
                            i += 3;
                            for (int j = 0; j < numBins; j++) {
                                ach[currentPos] = charArrayToInt(values, i+1, i+6);
                                currentPos++;
                                i += 5;
                            }

                        }
                    }
                }
                //Read in the tokens, with values. pass to function to add to ACH.
            }

        } catch (Exception nfe) {
            System.out.println("decoding error: " + nfe.getMessage());
            System.out.println(values.length);
            System.out.println(new String(values));
            System.out.println(doesItHit);
        }


        return new SpecChunk(ach , startPos, footprintDesc, footprintCode);
    }



    /** Converts an input chart array into a valid integer. 'i.e' 000000123, -> 123
     *  Must supply full character array and start and end of the section to convert.
     *  Avoids creating extra objects
     * @param data full character sequence of the input string, tokens and all.
     * @param start start of the sequence of convert
     * @param end end of the sequence to convert
     * @return valid int, or 0.
     * @throws NumberFormatException invalid digit, cannot be a number
     */
    private static int charArrayToInt(char[] data, int start, int end) throws NumberFormatException{
        int result = 0;
        for(int i = start; i < end; i++){
            int digit = (int)data[i] - (int)'0';
            if((digit < 0) || (digit > 9)) throw new NumberFormatException("cannot convert digit: " + digit);
            result *= 10;
            result += digit;
        }
        return result;
    }
}

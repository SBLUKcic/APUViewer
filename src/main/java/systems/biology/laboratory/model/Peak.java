package systems.biology.laboratory.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Peak extends Object{

    private final SimpleIntegerProperty peakStart;
    private final SimpleIntegerProperty peakEnd;
    private final SimpleIntegerProperty peakMaxima;
    private final SimpleIntegerProperty peakHeight;
    private final SimpleIntegerProperty peakWidth;
    private final SimpleStringProperty rname;

    public Peak(int peakStart, int peakEnd, int peakMaxima, int peakHeight, String rname) {
        this.peakStart = new SimpleIntegerProperty(peakStart);
        this.peakEnd = new SimpleIntegerProperty(peakEnd);
        this.peakMaxima = new SimpleIntegerProperty(peakMaxima);
        this.peakHeight = new SimpleIntegerProperty(peakHeight);
        this.peakWidth = new SimpleIntegerProperty(peakEnd - peakStart);
        this.rname = new SimpleStringProperty(rname);
    }

    public String getRname() {
        return rname.get();
    }

    public SimpleStringProperty rnameProperty() {
        return rname;
    }

    public int getPeakWidth() {
        return peakWidth.get();
    }

    public int getPeakHeight() {
        return peakHeight.get();
    }

    public int getPeakStart() {
        return peakStart.get();
    }

    public int getPeakEnd() {
        return peakEnd.get();
    }

    public int getPeakMaxima() {
        return peakMaxima.get();
    }

    @Override
    public String toString() {
        return "Peak{" +
                "peakStart= " + peakStart.get() +
                ", peakEnd= " + peakEnd.get() +
                ", peakHeight= " + peakHeight.get() +
                ", peakMaximaPos= " + peakMaxima.get() +
                ", rname= " + rname.get() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Peak peak = (Peak) o;

        if (peakMaxima.get() != peak.getPeakMaxima()) return false;
        return rname.get().equals(peak.rname.get());
    }

    @Override
    public int hashCode() {
        int result = peakMaxima.hashCode();
        result = 31 * result + rname.hashCode();
        return result;
    }
}

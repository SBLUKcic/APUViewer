package systems.biology.laboratory.model;

public enum Footprintcode {
    read(201, "Line footprint - reads "),
    span(202, "Line footprint - spans"),
    window(203, "Line footprint - window"),
    point(000, "point footprint");

    private int code;
    private String translation;

    Footprintcode(int code, String translation) {
        this.code = code;
        this.translation = translation;
    }

    public int getCode() {
        return code;
    }

    public String getTranslation() {
        return translation;
    }
}

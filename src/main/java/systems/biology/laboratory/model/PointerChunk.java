package systems.biology.laboratory.model;

public class PointerChunk {

    private int stepSize;
    private int binSize;
    private String rname;
    private int bytePosition;
    private String id;

    public PointerChunk(int stepSize, int binSize, String rname, int bytePosition) {
        this.stepSize = stepSize;
        this.binSize = binSize;
        this.rname = rname;
        this.bytePosition = bytePosition;
        this.id = stepSize + "_" + binSize + "_" + rname;
    }

    public String getId() {
        return id;
    }

    public int getStepSize() {
        return stepSize;
    }

    public int getBinSize() {
        return binSize;
    }

    public String getRname() {
        return rname;
    }

    public int getBytePosition() {
        return bytePosition;
    }

    public static String createId(int step, int bin, String rname){
        return step+"_"+bin+"_"+rname;
    }
}

package systems.biology.laboratory.model;

public class SpecChunk {

    private int[] histogram;
    private int startPos;
    private String footprintDescription;
    private String footprintCode;

    public SpecChunk(int[] histogram, int startPos, String footprintDescription, String footprintCode) {
        this.histogram = histogram;
        this.startPos = startPos;
        this.footprintDescription = footprintDescription;
        this.footprintCode = footprintCode;
    }

    public String getFootprintCode() {
        return footprintCode;
    }

    public String getFootprintDescription() {
        return footprintDescription;
    }


    public int[] getHistogram() {
        return histogram;
    }

    public int getStartPos() {
        return startPos;
    }
}

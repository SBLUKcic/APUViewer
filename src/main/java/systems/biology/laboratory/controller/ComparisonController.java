package systems.biology.laboratory.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import systems.biology.laboratory.components.*;
import systems.biology.laboratory.model.Peak;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.model.SpecChunk;
import systems.biology.laboratory.services.APUChunkReaderService;
import systems.biology.laboratory.services.PeakFinderService;
import systems.biology.laboratory.services.PeakListComparisonService;
import systems.biology.laboratory.utils.APUReader;

import java.io.File;
import java.util.*;

public class ComparisonController {

    /** boolean to capture whether to synchronize the peak lists. i.e clicking on a peak will search the area on both charts */
    private boolean arePeaksSynchronzied = false;

    /** Check for whether the scroll bars are sync'd across the charts */
    private boolean areScrollBarsSynchronized = false;

    /** tooltips for the areaChart */
    private Tooltip t;

    /** apu files */
    private File apuFileOne, apuFileTwo;

    /** peak lists corresponding to the two graphs */
    private List<Peak> peakListOne, peakListTwo;

    /** Loaded values to display on the area charts.
     *  Used throughout the controller for displaying values */
    private int[] chartValuesOne, chartValuesTwo;

    /** Starting position on the charts */
    private int chartOneStartPos, chartTwoStartPos;

    /** Current rname of the loaded chart values
     *  Used to differentiate between the need to load new chartValues, or just change the slider position and display the next values
     */
    private String currentRnameOne, currentRnameTwo;

    /** List of pointerChunks extracted from the start of the APU file.
     *  Displays the options from the file, and where to read the data
     */
    private Map<String, PointerChunk> pointerChunksOne, pointerChunksTwo;

    /** FXML ScrollBars */
    @FXML
    ScrollBar chartOneScrollBar, chartTwoScrollBar;

    @FXML
    AreaChart<Number, Number> chartOneAreaChart, chartTwoAreaChart;

    @FXML
    NumberAxis chartOneXAxis, chartOneYAxis, chartTwoXAxis, chartTwoYAxis;

    @FXML
    Pane chartOneRnameList, chartTwoRnameList, chartOneControls, chartTwoControls, chartOneInfoBox, chartTwoInfoBox;

    @FXML
    Tab chartOnePeakList, chartTwoPeakList;

    @FXML
    BorderPane chartOneBorderPane, chartTwoBorderPane;

    @FXML
    TabPane chartOneTabPane, chartTwoTabPane;

    @FXML
    Slider zoomOne, zoomTwo;


    /** File selector which initiates the peak and rname lists.
     *  User can choose one or two files, and assign each to a different area chart,
     *  Filter is applied to only show .apu files
     *  Bound to many @FXML annotated controls.
     */
    public void chooseFiles() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select 2 APU Files to compare");

        //extension filter, to select only .apu files
        FileChooser.ExtensionFilter extensionFilter = new ExtensionFilter("APU Files (*.apu)","*.apu");
        fileChooser.getExtensionFilters().add(extensionFilter);

        String userDirectoryString = System.getProperty("user.home");
        File userDirectory = new File(userDirectoryString);
        if(!userDirectory.canRead()){
            userDirectory = new File("c:/");
        }
        fileChooser.setInitialDirectory(userDirectory);

        try{
            List<File> apu = fileChooser.showOpenMultipleDialog(new Stage());
     
            for(File f : apu){
                if(!f.getName().endsWith(".apu")) throw new Exception("Please select a valid .apu file");
                int chartNumber = APUFileDetailsModalMultiple.display(f.getName(), f);
                if(chartNumber == 1){
                    apuFileOne = f;
                    System.out.println("here");
                    pointerChunksOne = APUReader.extractPointers(f);
                    initialiseRNameList(pointerChunksOne, 1, chartOneRnameList);
                    initialisePeakList(pointerChunksOne, 1, chartOnePeakList, chartOneTabPane);
                    initialiseChartControls(1, chartOneControls);

                }else if(chartNumber == 2){
                    System.out.println("2");
                    apuFileTwo = f;
                    pointerChunksTwo = APUReader.extractPointers(f);
                    initialiseRNameList(pointerChunksTwo, 2, chartTwoRnameList);
                    initialisePeakList(pointerChunksTwo, 2, chartTwoPeakList, chartTwoTabPane);
                    initialiseChartControls(2, chartTwoControls);
                }
            }
        }catch (Exception rte){
            MessageModal.display(rte.getMessage() == null ? "No file chosen" : rte.getMessage(), "Error choosing file");
        }
    }

    /** Initialise the control panel for working with the chart
     *  Contains
     *  <ul>
     *      <li>Search box for jumping around the chart</li>
     *      <li>more soon.</li>
     *  </ul>
     * @param chartNumber number of the chart to which the controls apply
     * @param controls Pane to render the controls to.
     */
    private void initialiseChartControls(int chartNumber, Pane controls) {
        VBox vBox = new VBox();
        vBox.prefWidthProperty().bind(controls.widthProperty());
        controls.setPrefWidth(100);
        Text findPos = new Text("Find position: ");
        TextField textField = new TextField();
        textField.setPromptText("Search chart..");


        textField.setOnKeyPressed(e -> {
            if(e.getCode().equals(KeyCode.ENTER)){
                try{
                    int searchValue = Integer.parseInt(textField.getText());
                    if(chartNumber == 1) {
                        if(searchValue <= chartOneStartPos + chartValuesOne.length-1) {
                            refreshAreaChart(searchValue, chartOneAreaChart.getTitle(), chartValuesOne[0], 1, 1, (int)zoomOne.getValue());
                            resetScrollBar(searchValue, chartOneScrollBar);
                        }
                    }else{
                        if(searchValue <= chartTwoStartPos + chartValuesTwo.length-1) {
                            refreshAreaChart(searchValue, chartTwoAreaChart.getTitle(), chartValuesTwo[0], 1, 2, (int)zoomTwo.getValue());
                            resetScrollBar(searchValue, chartTwoScrollBar);
                        }
                    }

                }catch (NumberFormatException ne ){
                    System.out.println(ne.getMessage());
                }
            }
        });

        vBox.getChildren().addAll(findPos, textField);

        if(chartNumber == 1) {
            Text syncText = new Text("sync peak lists: ");
            CheckBox peakCheckBox = new CheckBox();
            Text syncBarsText = new Text("sync scroll bars: ");
            CheckBox barsCheckBox = new CheckBox();

            barsCheckBox.setOnAction(e -> {
                areScrollBarsSynchronized = barsCheckBox.isSelected();
            });

            peakCheckBox.setOnAction(e -> {
                arePeaksSynchronzied = peakCheckBox.isSelected();
            });

            HBox hBox = new HBox();
            hBox.setPadding(new Insets(5,0,5,0));
            hBox.getChildren().addAll(syncText, peakCheckBox);

            HBox dhBox = new HBox();
            dhBox.setPadding(new Insets(5,0,5,0));
            dhBox.getChildren().addAll(syncBarsText, barsCheckBox);
            vBox.getChildren().addAll(hBox, dhBox);
            vBox.setMargin(peakCheckBox, new Insets(0,0,0,30));



        }





        vBox.setSpacing(5);
        vBox.setPadding(new Insets(5,5,5,5));

        controls.getChildren().add(vBox);
    }

    /** Initialise the peak table on the RHS of the graph
     * @param pointerChunks Pointerchunks extracted from the file
     * @param chartNumber chart number to apply the peak
     * @param peakTab Pane on the fxml file to load the table into
     */
    private void initialisePeakList(Map<String, PointerChunk> pointerChunks, int chartNumber, Tab peakTab, TabPane tabPane) {

        PeakFinderService peakFinderService = new PeakFinderService(pointerChunks, chartNumber == 1 ? apuFileOne : apuFileTwo);

        StackPane stackPane = new StackPane();
        ProgressBar progressBar = new ProgressBar();
        Label label = new Label();

        VBox box = new VBox(label, progressBar);

        peakFinderService.setOnSucceeded(e -> {
            stackPane.getChildren().remove(box);
            VBox peakBox = renderPeakTable(peakFinderService.getValue(), chartNumber);
            if(chartNumber == 1){
                peakBox.prefHeightProperty().bind(chartOneBorderPane.heightProperty());
                peakListOne = peakFinderService.getValue();
            }else{
                peakBox.prefHeightProperty().bind(chartTwoBorderPane.heightProperty());
                peakListTwo = peakFinderService.getValue();
            }
            peakBox.prefWidthProperty().bind(tabPane.widthProperty());
            stackPane.getChildren().add(peakBox);
        });

        label.textProperty().bind(peakFinderService.messageProperty());
        progressBar.progressProperty().bind(peakFinderService.progressProperty());


        stackPane.getChildren().addAll(box);
        StackPane.setAlignment(stackPane, Pos.CENTER);
        StackPane.setAlignment(box, Pos.CENTER);

        //bind the heights
        box.prefHeightProperty().bind(tabPane.prefHeightProperty());

        if(chartNumber == 1){ tabPane.prefHeightProperty().bind(chartOneBorderPane.heightProperty()); }
        else{ tabPane.prefHeightProperty().bind(chartTwoBorderPane.heightProperty()); }

        peakTab.setContent(stackPane);


        peakFinderService.start();
    }

    /** Displays peak data in a tableview
     * @param allPeaks peak data.
     * @return tableview - the table of data from the Peak Object
     */
    private VBox renderPeakTable(List<Peak> allPeaks, int chartNumber) {

        VBox vBox = new VBox();

        ObservableList<Peak> peaks = FXCollections.observableArrayList(allPeaks);
        //instantiate the table
        TableView tableView = new TableView();
        tableView.setId(String.valueOf(chartNumber));
        //start values
        TableColumn startValue = new TableColumn("Start pos");
        startValue.setMinWidth(100);
        startValue.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("peakStart"));

        TableColumn maximumValue = new TableColumn("Max pos");
        maximumValue.setMinWidth(100);
        maximumValue.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("peakMaxima"));

        TableColumn endValue = new TableColumn("End pos");
        endValue.setMinWidth(100);
        endValue.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("peakEnd"));

        TableColumn peakMaximaHeight = new TableColumn("Height");
        peakMaximaHeight.setMinWidth(100);
        peakMaximaHeight.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("peakHeight"));

        TableColumn peakWidth = new TableColumn("Width");
        peakWidth.setMinWidth(100);
        peakWidth.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("peakWidth"));

        TableColumn rName = new TableColumn("rname");
        rName.setMinWidth(200);
        rName.setCellValueFactory(new PropertyValueFactory<Peak, Integer>("rname"));



        tableView.getColumns().addAll(startValue, maximumValue, endValue, peakMaximaHeight, peakWidth, rName);

        tableView.setRowFactory(tv -> {
            TableRow<Peak> row = new TableRow<>();
            row.setOnMouseClicked(event ->{
                if(!row.isEmpty() && event.getButton()== MouseButton.PRIMARY
                        && event.getClickCount() == 2){
                    Peak pc = row.getItem();
                    selectPeak(pc, Integer.parseInt(tableView.getId()));
                }
            });
            return row;
        });

        //set the filter
        FilteredList<Peak> filteredData = new FilteredList<>(peaks, s -> true);

        TextField filterInput = new TextField();
        filterInput.setPromptText("Search...");

        filterInput.textProperty().addListener(((observable, oldValue, newValue) -> {

            filteredData.setPredicate(peak -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if(peak.getRname().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                }

                return false;
            });

        }));

        SortedList<Peak> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tableView.comparatorProperty());

        tableView.setItems(sortedData);

        tableView.prefHeightProperty().bind(vBox.prefHeightProperty());
        vBox.getChildren().addAll(filterInput, tableView);

        return vBox;

    }

    /** Selects peak double-clicked on in the peak list
     * @param peak peak selected
     * @param chartNumber peak list selected on, render the result onto
     */
    private void selectPeak(Peak peak, int chartNumber) {

        final String BIN_ONE_STEP_ONE_RES = "1_1_";

        int numIterations = arePeaksSynchronzied ? 2 : 1;

        for(int i = 0; i < numIterations; i++) {

            if (i == 1) chartNumber = chartNumber == 1 ? 2 : 1;

            final String currentRname = chartNumber == 1 ? currentRnameOne : currentRnameTwo;
            final Map<String, PointerChunk> pointerChunks = chartNumber == 1 ? pointerChunksOne : pointerChunksTwo;
            final File APUFile = chartNumber == 1 ? apuFileOne : apuFileTwo;
            final ScrollBar areaChartBar = chartNumber == 1 ? chartOneScrollBar : chartTwoScrollBar;
            final AreaChart<Number, Number> areaChart  = chartNumber == 1 ? chartOneAreaChart : chartTwoAreaChart;
            final NumberAxis xAxis  = chartNumber == 1 ? chartOneXAxis : chartTwoXAxis;
            final NumberAxis yAxis = chartNumber == 1 ? chartOneYAxis : chartTwoYAxis;
            final Slider slider  = chartNumber == 1 ? zoomOne : zoomTwo;
            final int[] currentValues = chartNumber == 1 ? chartValuesOne : chartValuesTwo;
            final int startPos = chartNumber == 1 ? chartOneStartPos : chartTwoStartPos;

            //look for the 1,1 resolution rname chunk
            String id = BIN_ONE_STEP_ONE_RES + peak.getRname();

                PointerChunk chunk = pointerChunks.get(id);
                if (currentRname == null || !currentRname.equals(chunk.getRname())) {
                    APUChunkReaderService service = new APUChunkReaderService(chunk, APUFile);
                    final int num = chartNumber;

                    //set succeed action
                    service.setOnSucceeded(e -> {
                        SpecChunk specChunk = service.getValue();
                        if (num == 1) {
                            chartValuesOne = specChunk.getHistogram();
                            currentRnameOne = chunk.getRname();
                            chartOneStartPos = specChunk.getStartPos();
                            initialiseInfoBox(chartOneInfoBox, specChunk.getFootprintDescription(), currentRnameOne, apuFileOne.getName(), specChunk.getStartPos() + specChunk.getHistogram().length, specChunk.getStartPos());
                            initialiseAreaChart(areaChart, chartValuesOne, peak.getRname(), 1, specChunk.getStartPos(), xAxis, yAxis, areaChartBar, slider, 1);
                            resetScrollBar(peak.getPeakStart(), areaChartBar);
                        } else {
                            chartValuesTwo = specChunk.getHistogram();
                            currentRnameTwo = chunk.getRname();
                            chartTwoStartPos = specChunk.getStartPos();
                            initialiseInfoBox(chartTwoInfoBox, specChunk.getFootprintDescription(), currentRnameTwo, apuFileTwo.getName(), specChunk.getStartPos() + specChunk.getHistogram().length, specChunk.getStartPos());
                            initialiseAreaChart(areaChart, chartValuesTwo, peak.getRname(), 1, specChunk.getStartPos(), xAxis, yAxis, areaChartBar, slider, 2);
                            resetScrollBar(peak.getPeakStart(), areaChartBar);
                        }
                    });

                    service.setOnFailed(e -> {
                        MessageModal.display("Error loading peak", "error");
                    });

                    service.start();

                }else{
                    initialiseAreaChart(areaChart, currentValues, peak.getRname(), 1, startPos, xAxis, yAxis, areaChartBar, slider, chartNumber);
                    resetScrollBar(peak.getPeakStart(), areaChartBar);
                }
        }
    }

    private void initialiseInfoBox(Pane info, String footprintDescription, String rname, String fileName, int max, int min) {

        info.getChildren().clear();

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(5,5,5,5));
        vBox.setSpacing(5);

        Text heading = new Text("Info: " + footprintDescription);

        Text rnameHeader = new Text("rname: " + rname);

        Text fileHeader = new Text("File: " + fileName);

        Text minText = new Text("Min pos: " + min);

        Text maxPos = new Text("Max pos: " + max);

        vBox.getChildren().addAll(heading, rnameHeader, fileHeader, minText, maxPos);

        vBox.setMinWidth(400);

        info.getChildren().add(vBox);

    }

    /** resets the value of the scroll bar.
     * @param value new value of the bar
     * @param areaChartBar the bar to change the value of
     */
    private void resetScrollBar(int value, ScrollBar areaChartBar) {
        //reset the scroll bar
        areaChartBar.setValue(value);
    }

    /** loads the data into the area chart between the lower bound and the upper bound.
     * @param title title of the chart
     */
    private void refreshAreaChart(int startPos, String title, int firstBin, int stepsize, int chartNumber, int scopeSize) {

        AreaChart areaChart;
        int[] chartValues;
        NumberAxis yAxis;
        NumberAxis xAxis;
        if(chartNumber == 1){
            areaChart = chartOneAreaChart;
            chartValues = chartValuesOne;
            xAxis = chartOneXAxis;
            yAxis = chartOneYAxis;
        }else{
            xAxis = chartTwoXAxis;
            yAxis = chartTwoYAxis;
            chartValues = chartValuesTwo;
            areaChart = chartTwoAreaChart;
        }
        areaChart.getData().clear();
        XYChart.Series<Number, Number> seriesACH= new XYChart.Series<>();

        seriesACH.setName(title);
        areaChart.setTitle(title);


        int endCoord = 0;
        int endPos = 0; //limit to scopeSize values

        int maxFrequency = 0;

        startPos = startPos >= firstBin ? startPos : firstBin;

        int start = (startPos - firstBin)/stepsize;

        for(int i = start; i < chartValues.length; i++){ //start at the right bin position, loop only 200 values

            seriesACH.getData().add(new XYChart.Data<>(firstBin + (i*stepsize), chartValues[i]));
            endPos++;
            endCoord = firstBin + (i*stepsize);

            if(endPos == scopeSize){ //last point, get the key + break to stop it
                break;
            }

        }
        areaChart.getData().addAll(Collections.singleton(seriesACH));

        yAxis.setLowerBound(0);
        yAxis.setUpperBound(maxFrequency);

        //List<EncodingValues> reverseRunLengthEncoding

        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(startPos);
        xAxis.setUpperBound(endCoord);

        initialiseTooltips(seriesACH);
    }

    private void initialiseRNameList(Map<String, PointerChunk> pointerChunks, int chartNumber, Pane pane) {

        Set<String> pointerSet = new HashSet<>();
        for(Map.Entry<String, PointerChunk> chunkEntry : pointerChunks.entrySet()){
            pointerSet.add(chunkEntry.getValue().getRname());
        }

        //content
        Text t = new Text("Rnames:");
        ObservableList<HBox> boxList = FXCollections.observableArrayList();
        for(String s : pointerSet){
            HBox hbox = new HBox();
            Button button = new Button("Load");
            button.setOnAction((ActionEvent e ) -> loadAPUChunk(button.getId().substring(2), pointerChunks, chartNumber == 1 ? apuFileOne : apuFileTwo, Integer.parseInt(button.getId().substring(0,1))));
            button.setId(chartNumber + "_" + s);
            Text text = new Text(s);
            final Pane spacer = new Pane();
            HBox.setHgrow(spacer, Priority.ALWAYS);
            spacer.setMinSize(10,1);
            hbox.getChildren().addAll(text, spacer, button);
            boxList.add(hbox);
        }

        FilteredList<HBox> filteredData = new FilteredList<>(boxList, s -> true);

        TextField filterInput = new TextField();
        filterInput.setPromptText("Search...");
        filterInput.textProperty().addListener(obs->{
            String filters = filterInput.getText();
            if(filters == null || filters.length() == 0) {
                filteredData.setPredicate(s -> true);
            }
            else {
                filteredData.setPredicate(s -> {
                    Text text = (Text)s.getChildren().get(0);
                    return text.getText().toLowerCase().contains(filters.toLowerCase());
                });
            }
        });

        VBox vBox = new VBox();
        ListView<HBox> listView = new ListView<>(filteredData);
        vBox.getChildren().addAll(t, filterInput, listView);


        //bind the heights
        listView.prefHeightProperty().bind(vBox.prefHeightProperty());
        vBox.prefHeightProperty().bind(pane.prefHeightProperty());

        if(chartNumber == 1){ pane.prefHeightProperty().bind(chartOneBorderPane.heightProperty()); }
        else{ pane.prefHeightProperty().bind(chartTwoBorderPane.heightProperty()); }

        pane.getChildren().add(vBox);


    }

    /** Loads the first APU chunk found matching the rname given
     * @param name rname to match
     */
    @FXML
    private void loadAPUChunk(String name, Map<String, PointerChunk> pointerChunks, File APUFile, int chartNumber) {


        for(Map.Entry<String, PointerChunk> pc : pointerChunks.entrySet()){
            if(pc.getValue().getRname().equals(name) && pc.getValue().getStepSize() == 1 && pc.getValue().getBinSize() == 1){
                Service<SpecChunk> loadThread = new APUChunkReaderService(pc.getValue(), APUFile);

                loadThread.setOnSucceeded(e -> {
                    if(chartNumber == 1) {
                        chartValuesOne = loadThread.getValue().getHistogram();
                        currentRnameOne = name;
                        //clear the data
                        chartOneAreaChart.getData().clear();
                        chartOneStartPos = loadThread.getValue().getStartPos();
                    }else{
                        chartValuesTwo = loadThread.getValue().getHistogram();
                        currentRnameTwo = name;
                        //clear the data
                        chartTwoAreaChart.getData().clear();
                        chartTwoStartPos = loadThread.getValue().getStartPos();
                    }

                    initialiseAreaChart(chartNumber == 1? chartOneAreaChart : chartTwoAreaChart,
                            chartNumber == 1 ? chartValuesOne : chartValuesTwo,
                            pc.getValue().getId(), pc.getValue().getStepSize(),
                            loadThread.getValue().getStartPos(),
                            chartNumber == 1 ? chartOneXAxis : chartTwoXAxis,
                            chartNumber == 1 ? chartOneYAxis : chartTwoYAxis,
                            chartNumber == 1 ? chartOneScrollBar : chartTwoScrollBar,
                            chartNumber == 1 ? zoomOne : zoomTwo,
                            chartNumber);
                });


                loadThread.setOnFailed(e -> {
                    MessageModal.display("Could not display current chunk", "Error");
                });

                loadThread.start();
            }
        }


    }

    /** Initialises the input area chart with data specified
     * @param areaChart FXML AreaChart to populate
     * @param chartValues values from the file to load
     * @param title Title of the AreaChart - usually an Rname.
     * @param stepsize Step size, default is 1.
     * @param firstBin First bin, read from the Token in the .apu file
     * @param xAxis NumberAxis of integers on the x axis
     * @param yAxis NumberAxis of integers on the y axis
     * @param areaChartBar ScrollBar assigned to the AreaChartt
     * @param chartNumber AreaChart number. 1 or 2.
     */
    private void initialiseAreaChart(AreaChart<Number, Number> areaChart, int[] chartValues, String title, int stepsize, int firstBin, NumberAxis xAxis, NumberAxis yAxis, ScrollBar areaChartBar, Slider slider, int chartNumber) {
        //reset the chart
        areaChart.getData().clear();
        areaChart.setAnimated(false);
        areaChart.setLegendVisible(false);
        areaChart.setHorizontalGridLinesVisible(false);
        areaChart.setVerticalGridLinesVisible(false);

        XYChart.Series<Number, Number> seriesACH = new XYChart.Series<>();
        //set name
        seriesACH.setName(title);


        boolean firstPass = true;
        int startCoord = 0;
        int endCoord = 0;
        int realMax = 0;
        int endPos = 0; //limit to 1000 values
        for(int i = 0; i < chartValues.length; i++){
            endPos++;
            if (endPos < 200) {
                if (firstPass) {
                    startCoord = firstBin + (i*stepsize);
                    firstPass = false;
                }
                XYChart.Data<Number, Number> data = new XYChart.Data<>(firstBin + (i*stepsize), chartValues[i]);

                seriesACH.getData().add(data);
                endCoord = firstBin + (i*stepsize);
            }else{
                break;
            }

        }

        realMax = firstBin + ((chartValues.length - 1) * stepsize);

        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(startCoord);
        xAxis.setUpperBound(endCoord);
        xAxis.setTickLabelGap(stepsize/2);
        xAxis.setMinorTickVisible(false);
        xAxis.setTickMarkVisible(false);
        xAxis.setTickLabelRotation(90.0);


        areaChartBar.setVisible(true);
        areaChartBar.setMin(startCoord);
        areaChartBar.setMax(realMax);
        areaChartBar.setValue(startCoord);
        areaChartBar.setUnitIncrement(stepsize);

        areaChartBar.setUnitIncrement(10.0);

        areaChartBar.valueProperty().addListener((observable, oldValue, newValue) -> {
            refreshAreaChart((int) areaChartBar.getValue() - (int)(slider.getValue()/2), title, firstBin, stepsize, chartNumber, (int)slider.getValue());
            yAxis.setMinorTickLength(1);
            if(areScrollBarsSynchronized){
                if(chartNumber == 1){
                    chartTwoScrollBar.setValue(chartOneScrollBar.getValue());
                }else{
                    chartOneScrollBar.setValue(chartTwoScrollBar.getValue());
                }
            }
        });

        slider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            refreshAreaChart((int) areaChartBar.getValue() - (int)(slider.getValue()/2), title, firstBin, stepsize, chartNumber, (int)slider.getValue());
            yAxis.setMinorTickLength(1);
        }));

        areaChart.setTitle(title);
        areaChart.getData().addAll(Collections.singleton(seriesACH));
        initialiseTooltips(seriesACH);

    }

    /** exit option from the File menu
     * @param actionEvent actionEvent
     */
    public void exitProgram(ActionEvent actionEvent) {
        System.exit(0);
    }

    /** Extracts the chosen peak list from one of the area charts into CSV format.
     * @param actionEvent actionEvent
     */
    public void extractPeakList(ActionEvent actionEvent) {
        int answer = MultiChoiceModal.display("Select a peaklist", "Peak List Extraction", "Peak list 1", "Peak list 2");
        if(answer != 0){ //ignore on cancel
            StackPane stackPane;
            if(answer == 1) {
                stackPane = (StackPane) chartOnePeakList.getContent();
            }else{
                stackPane = (StackPane) chartTwoPeakList.getContent();
            }
            VBox vBox = (VBox)stackPane.getChildren().get(0);
            TableView tableView = (TableView)vBox.getChildren().get(2);
            if(tableView != null){
                String fileName = StringModal.display("Enter a filename", "File name selection");
                if(!fileName.endsWith(".csv")){
                    fileName += ".csv";
                }
                File file = new File(System.getProperty("user.dir") + File.separator + fileName);
                try {
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("Start Position, Max Position, End Position, Peak Height, Peak Width, Rname");
                    ObservableList<Peak> peaks = (ObservableList<Peak>) tableView.getItems();
                    for (Peak p : peaks) {
                        pw.println(String.format("%s,%s,%s,%s,%s,%s", p.getPeakStart(), p.getPeakMaxima(), p.getPeakEnd(), p.getPeakHeight(), p.getPeakWidth(), p.getRname()));
                    }

                    pw.flush();
                    pw.close();

                }catch (IOException ioe){
                    System.out.println(ioe.getMessage());
                }
            }
        }
    }

    /** initialises the tooltips which are displayed when the mouse is hovered over or clicked on the area chart points
     * @param seriesACH data series to add the tooltips to.
     */
    private void initialiseTooltips(XYChart.Series<Number, Number> seriesACH) {
        seriesACH.getData().forEach(e -> {
            e.getNode().addEventFilter(MouseEvent.MOUSE_ENTERED_TARGET, evt -> {
               /* Tooltip */
                t = new Tooltip();
                t.setText("pos: " + e.getXValue() + "\n" + "height: " + e.getYValue());
                t.show(e.getNode(),
                        (int) e.getNode().localToScreen(e.getNode().getLayoutBounds()).getMaxX(),
                        (int) e.getNode().localToScreen(e.getNode().getLayoutBounds()).getMaxY());

            });

            e.getNode().addEventFilter(MouseEvent.MOUSE_EXITED, evt -> {
                if(t != null && t.isShowing()){
                    t.hide();
                }
            });
        });
    }

    //TODO: find out what actionEvents are useful for.
    public void comparePeakLists() {

        int threshold = IntModal.display("Please select the threshold for comparing peaks", "Peak Difference");

        PeakListComparisonService service = new PeakListComparisonService(peakListOne, peakListTwo, threshold);

        service.setOnSucceeded(e -> {
            System.out.println("done");
            VBox newTabContent = renderPeakTable(service.getValue(), 1);
            Tab tab = new Tab("Interesting peaks");
            tab.setContent(newTabContent);

            chartOneTabPane.getTabs().add(tab);
        });

        service.start();
    }
}

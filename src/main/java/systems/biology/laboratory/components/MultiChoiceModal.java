package systems.biology.laboratory.components;

import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by PhysicsSam on 09-Apr-18.
 */
public class MultiChoiceModal {

  static int answer;

  public static int display(String message, String title, String... choices){
    Stage window = new Stage();

    //Block events to other windows
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle(title);
    window.setMinWidth(400);
    window.setMinHeight(400);
    window.setMaxHeight(500);

    List<Button> buttonList = new ArrayList<>();
    int buttonCounter = 1;
    for(String choice : choices){
      Button choiceButton = new Button(choice);
      int counting = buttonCounter;

      choiceButton.setOnAction( e -> {
          answer = counting;
          window.close();
      });

      buttonCounter++;

      buttonList.add(choiceButton);
    }

    Button cancelButton = new Button("Cancel");

    cancelButton.setOnAction( e -> {
      answer = 0;
      window.close();
    });

    buttonList.add(cancelButton);

    BorderPane layout = new BorderPane();
    HBox buttonBox = new HBox(10);
    buttonBox.setPadding(new Insets(0,10,10,10));
    buttonBox.getChildren().addAll(buttonList);
    buttonBox.setAlignment(Pos.CENTER);

    Text text = new Text(message);

    //add text
    layout.setCenter(text);


    //add buttons
    layout.setBottom(buttonBox);

    //layout.setAlignment(Pos.CENTER);
    Scene scene = new Scene(layout);
    window.setScene(scene);
    window.showAndWait();

    return answer;
  }



}

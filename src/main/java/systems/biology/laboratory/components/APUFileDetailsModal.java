package systems.biology.laboratory.components;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import systems.biology.laboratory.utils.APUReader;
import systems.biology.laboratory.model.PointerChunk;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class APUFileDetailsModal {

    static boolean answer;

    public static boolean display(String title, File apuFile){
        Stage window = new Stage();

        //Block events to other windows
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(400);
        window.setMinHeight(400);
        window.setMaxHeight(500);

        Map<String, PointerChunk> pointerChunks = APUReader.extractPointers(apuFile);

        //content
        ScrollPane scrollPane = new ScrollPane();
        Text text = new Text();
        Set<String> pointerSet = new HashSet<>();
        for(Map.Entry<String, PointerChunk> chunkEntry : pointerChunks.entrySet()){
            pointerSet.add(chunkEntry.getValue().getRname());
        }

        StringBuilder sb = new StringBuilder();
        for(String s : pointerSet){
            sb.append(s);
            sb.append("\n");
        }

        text.setText(sb.toString());
        scrollPane.setContent(text);

        //Cancel and Load buttons
        Button loadButton = new Button("Load");
        Button cancelButton = new Button("Cancel");

        loadButton.setOnAction(e -> {
            answer = true;
            window.close();
        });

        cancelButton.setOnAction(e -> {
            answer = false;
            window.close();
        });

        BorderPane layout = new BorderPane();
        HBox buttonBox = new HBox(10);
        buttonBox.getChildren().addAll(loadButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        //add buttons
        layout.setCenter(scrollPane);
        layout.setBottom(buttonBox);

        //layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}

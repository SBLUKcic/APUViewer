package systems.biology.laboratory.components;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import systems.biology.laboratory.model.PointerChunk;
import systems.biology.laboratory.utils.APUReader;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class APUFileDetailsModalMultiple {

    static int answer;

    public static int display(String title, File apuFile){
        Stage window = new Stage();

        //Block events to other windows
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(400);
        window.setMinHeight(400);
        window.setMaxHeight(500);

        Map<String, PointerChunk> pointerChunks = APUReader.extractPointers(apuFile);

        //content
        ScrollPane scrollPane = new ScrollPane();
        Text text = new Text();
        Set<String> pointerSet = new HashSet<>();
        for(Map.Entry<String, PointerChunk> chunkEntry : pointerChunks.entrySet()){
            pointerSet.add(chunkEntry.getValue().getRname());
        }

        StringBuilder sb = new StringBuilder();
        for(String s : pointerSet){
            sb.append(s);
            sb.append("\n");
        }

        text.setText(sb.toString());
        scrollPane.setContent(text);

        //Cancel and Load buttons
        Button oneButton = new Button("Chart 1");
        Button twoButton = new Button("Chart 2");
        Button cancelButton = new Button("Cancel");

        oneButton.setOnAction(e -> {
            answer = 1;
            window.close();
        });

        twoButton.setOnAction(e -> {
            answer = 2;
            window.close();
        });

        cancelButton.setOnAction(e -> {
            answer = 0;
            window.close();
        });

        BorderPane layout = new BorderPane();
        HBox buttonBox = new HBox(10);
        buttonBox.getChildren().addAll(oneButton, twoButton, cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        //add buttons
        layout.setCenter(scrollPane);
        layout.setBottom(buttonBox);

        //layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}

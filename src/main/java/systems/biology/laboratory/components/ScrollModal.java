package systems.biology.laboratory.components;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import systems.biology.laboratory.model.Peak;

import java.util.List;

public class ScrollModal {

    public static void display(List<Peak> list, String title){

        Stage window = new Stage();

        //Block events to other windows
        window.initModality(Modality.NONE);
        window.setTitle(title);
        window.setMinWidth(400);
        window.setMinHeight(400);
        window.setMaxHeight(500);


        //content
        ScrollPane scrollPane = new ScrollPane();
        Text text = new Text();

        StringBuilder sb = new StringBuilder();
        for(Object object : list){
            sb.append(object.toString());
            sb.append("\n");
        }

        text.setText(sb.toString());
        scrollPane.setContent(text);

        //Cancel and Load buttons
        Button close = new Button("close");

        close.setOnAction(e -> {
            window.close();
        });

        BorderPane layout = new BorderPane();
        HBox buttonBox = new HBox(10);
        buttonBox.getChildren().addAll(close);
        buttonBox.setAlignment(Pos.CENTER);

        //add buttons
        layout.setCenter(scrollPane);
        layout.setBottom(buttonBox);

        //layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();

    }
}

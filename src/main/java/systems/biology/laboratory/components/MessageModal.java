package systems.biology.laboratory.components;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MessageModal {

    public static void display(String message, String title){
        Stage modal = new Stage();

        //Block events to other windows
        modal.initModality(Modality.APPLICATION_MODAL);
        modal.setTitle(title);
        modal.setMinWidth(400);
        modal.setMinHeight(100);
        modal.setMaxHeight(200);

        //Cancel and Load buttons
        Button okButton = new Button("Ok");
        okButton.setPadding(new Insets(3,10,3,10));

        okButton.setOnAction(e -> {
            modal.close();
        });

        BorderPane layout = new BorderPane();
        HBox buttonBox = new HBox(10);
        buttonBox.setPadding(new Insets(0,10,10,10));
        buttonBox.getChildren().addAll(okButton);
        buttonBox.setAlignment(Pos.CENTER);

        Text text = new Text(message);

        //add text
        layout.setCenter(text);

        //add buttons
        layout.setBottom(buttonBox);

        //layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        modal.setScene(scene);
        modal.showAndWait();
    }
}

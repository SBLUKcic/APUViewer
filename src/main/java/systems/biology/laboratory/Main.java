package systems.biology.laboratory;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        initialSetup(primaryStage);
    }

    private void initialSetup(Stage primaryStage) throws java.io.IOException {
        Parent root = FXMLLoader.load(Main.class.getResource("/fxml/comparisonViewer.fxml"));
        //Parent root = loader.load();
        primaryStage.setTitle("Peak Viewer");
        Scene scene = new Scene(root, 1500, 850);
        primaryStage.setMaximized(true);

        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/static/images/peak.png")));
        primaryStage.show();

        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
